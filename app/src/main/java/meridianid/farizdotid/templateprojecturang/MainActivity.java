package meridianid.farizdotid.templateprojecturang;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import meridianid.farizdotid.templateprojecturang.helper.SharedPrefManager;
import meridianid.farizdotid.templateprojecturang.helper.UIHelper;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    
    private EditText et_email;
    private Button button_validate;
    private Button btnCheckConnection;
    private EditText etName;
    private EditText etAge;
    private Button btnSave;

    SharedPrefManager spManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spManager = new SharedPrefManager(this);

        initComponents();
    }

    private void initComponents(){
        et_email = (EditText) findViewById(R.id.et_email);

        button_validate = (Button) findViewById(R.id.button_validate);
        button_validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UIHelper.validateEmail(et_email.getText().toString())){
                    Toasty.success(MainActivity.this, "valid email", Toast.LENGTH_SHORT).show();
                } else {
                    et_email.setError("invalid email");
                }
            }
        });

        btnCheckConnection = (Button) findViewById(R.id.btnCheckConnection);
        btnCheckConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();
            }
        });

        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UIHelper.isAlreadyFilled(etName, etAge)){
                    Log.i(TAG, "onClick: SUDAH TERISI");
                    spManager.setSPName(etName.getText().toString());
                    spManager.setSpAge(etAge.getText().toString());

                    Toasty.success(MainActivity.this, "Hello " + spManager.getSPName() +
                    ", Your Age : " + spManager.getSPAge(), Toast.LENGTH_LONG).show();
                } else {
                    Log.i(TAG, "onClick: BELUM TERISI");
                }
            }
        });
    }

    private void checkConnection(){
        if (UIHelper.checkInternet(MainActivity.this)){
            Toasty.success(MainActivity.this, "ADA INTERNET", Toast.LENGTH_SHORT).show();
        } else {
            Toasty.warning(MainActivity.this, "TIDAK ADA KONEKSI", Toast.LENGTH_SHORT).show();
        }
    }
}
