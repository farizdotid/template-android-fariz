package meridianid.farizdotid.templateprojecturang.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by fariz ramadhan.
 * website : www.farizdotid.com
 * github : https://github.com/farizdotid
 */
public class SharedPrefManager {

    public static final String SP_PROJECT_NAME = "spProjectName";
    public static final String SP_NAME = "spName";
    public static final String SP_AGE = "spAge";

    protected Context mContext;
    SharedPreferences sp;
    protected SharedPreferences.Editor spEditor;

    public SharedPrefManager(){
    }

    public SharedPrefManager(Context context){
        this.mContext = context;
        sp = context.getSharedPreferences(SP_PROJECT_NAME, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void setSPName(String name){
        spEditor.putString(SP_NAME, name);
        spEditor.commit();
    }

    public void setSpAge(String age){
        spEditor.putString(SP_AGE, age);
        spEditor.commit();
    }

    public String getSPName(){
        return sp.getString(SP_NAME, "");
    }

    public String getSPAge(){
        return sp.getString(SP_AGE, "");
    }
}