package meridianid.farizdotid.templateprojecturang.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatDrawableManager;
import android.view.View;
import android.widget.EditText;

/**
 * Created by farizdotid.
 * www.farizdotid.com
 */

public class UIHelper {

    public static boolean validateEmail(String email){
        boolean validate = true;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern)){
            validate = true;
        } else if (email.matches(emailPattern2)){
            validate = true;
        } else {
            validate = false;
        }

        return validate;
    }

    public static boolean isAlreadyFilled(EditText et1, EditText et2){
        boolean alreadyFilled;
        if (et1.getText().toString().isEmpty()){
            alreadyFilled = false;
            et1.setError("Isi Terlebih Dahulu");
        } else if (et2.getText().toString().isEmpty()){
            alreadyFilled = false;
            et2.setError("Isi Terlebih Dahulu");
        } else {
            alreadyFilled = true;
        }

        return alreadyFilled;
    }

    public static boolean checkInternet(Context context){
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager=(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
           connectStatus = true;
        }
        else {
           connectStatus = false;
        }
        return connectStatus;
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

}